fn main() {
    let mut buf= &mut String::new();
    let mut res: u32 = 0;

    std::io::stdin().read_line(&mut buf).expect("err");

    let n: u8 = buf.trim().parse::<u8>().unwrap();

    for _i in 0..n {
        let mut buf= &mut String::new();
        std::io::stdin().read_line(&mut buf).unwrap();

        let base: u32 = buf.trim().parse::<u32>().unwrap();

        res += u32::pow(base / 10, base%10);
    }

    println!("{}", res);
}
